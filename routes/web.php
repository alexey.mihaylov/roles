<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route ::get ( '/', function () {
    return view ( 'welcome' );
} );

Auth ::routes ();

Route ::group ( [ 'namespace' => 'Admin', 'prefix' => '/admin', 'as' => 'admin.' ], function () {
    Route ::get ( 'home', 'HomeController@index' ) -> name ( 'home' ) -> middleware ( [ 'auth', 'role:admin' ] );
    Route ::get ( 'login', 'LoginController@showLoginForm' ) -> name ( 'login' );
    Route ::post ( 'login', 'LoginController@login' ) -> name ( 'performLogin' );
} );

Route ::group ( [ 'namespace' => 'Member', 'prefix' => '/members', 'as' => 'member.' ], function () {
    Route ::get ( 'home', 'HomeController@index' ) -> name ( 'home' ) -> middleware ( [ 'auth', 'role:admin,member' ] );
    Route ::get ( 'login', 'LoginController@showLoginForm' ) -> name ( 'login' );
    Route ::post ( 'login', 'LoginController@login' ) -> name ( 'performLogin' );
} );

Route ::get ( '/home', 'HomeController@index' ) -> name ( 'home' );
