<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        $roleNames = [ 'registered', 'member', 'admin' ];

        foreach ( $roleNames as $roleName ) {
            Role ::create ( [ 'name' => $roleName ] );
        }
    }
}
