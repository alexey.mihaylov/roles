<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        $registeredRole = Role ::whereName ( 'registered' ) -> first ();

        factory ( User::class, 10 ) -> create () -> each (
            function ( $user ) use ( $registeredRole ) {
                /**
                 * @var $user User
                 */
                $user -> roles () -> save ( $registeredRole );
            }
        );
    }
}
