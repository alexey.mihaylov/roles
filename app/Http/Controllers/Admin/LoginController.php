<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct ()
    {
        $this -> middleware ( 'guest' ) -> except ( 'logout' );
    }

    public function redirectTo ()
    {
        return route('admin.home');
    }

    public function showLoginForm()
    {
        return view('admin.login');
    }

    public function username ()
    {
        return 'phone_number';
    }
}
