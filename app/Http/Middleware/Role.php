<?php

namespace App\Http\Middleware;

use Closure;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param array $roleNames
     * @return mixed
     */
    public function handle ( $request, Closure $next, ...$roleNames )
    {
        if ( ! $request -> user () -> hasOneOfRoles ( $roleNames ) ) {
            return redirect ( 'home' );
        }

        return $next( $request );
    }
}
